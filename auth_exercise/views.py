from django.contrib.auth.models import User
from rest_framework import viewsets, status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from auth_exercise.serializers import UserSerializer

from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
@api_view(['GET', 'POST'])
def index(request):
    return HttpResponse("Hello, world. This is the app index")

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

@api_view(['POST'])
@authentication_classes(())
@permission_classes(())
def create_auth(request):
    """
    API endpoint to create new users

    Args:
        request:

    Returns:
        Response: contains either user details if successful or error message
    """
    serialized = UserSerializer(data=request.data, context={'request': request})
    if serialized.is_valid():
        User.objects.create_user(
            email=serialized.initial_data['email'],
            username=serialized.initial_data['username'],
            password=serialized.initial_data['password'] # using init_data because this is not part of the serializer
        )
        return Response(serialized.validated_data, status=status.HTTP_201_CREATED)
    else:
        return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)