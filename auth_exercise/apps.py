from django.apps import AppConfig


class AuthExerciseConfig(AppConfig):
    name = 'auth_exercise'
