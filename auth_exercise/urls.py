from django.conf.urls import url
from rest_framework.authtoken import views as token_views

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login$', token_views.obtain_auth_token),
]